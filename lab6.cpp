// John Sadik
// Lab6
// This is a lab to output speeding tickets and fines (based on roads) between a set of dates in a specific format
 
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

const int ARRAY_SIZE = 13;

using namespace std;

int main()
{
  // Declaring our variables
  int month_s; // Starting month
  int day_s; // Starting day
  int year_s; // Starting year
  int month_e; // Ending month
  int day_e; // Ending day
  int year_e; // Ending year
  int month; // Month from file
  int day; // Day from file
  int year; // Year from file
  int speed_l; // Speed limit
  int speed; // Actual speed 
  const double i = 5.2252; // Interstate multiplier 
  const double h = 9.4412 ; // Highway multiplier
  const double r = 17.1525; // Residential multiplier
  const double n = 12.152; // Multiplier for none of the above
  double fine = 0.00;            
  char road; // Road type
  string file_t; // Ticket file
  string file_r; // Report file
  string citation;
  bool indate = false;
  string m_name[ARRAY_SIZE] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec", "Err"};
  ifstream fin;
  
  
  // Asking for ticket file
  cout << "Enter a ticket file: ";
  cin >> file_t;
  fin.open(file_t);
  // If we are unable to open file, end program with error message
  if (!fin)
  {
    cout << "Unable to open " << file_t << ".\n";
    return 0;
  }
  
  // Asking for output file
  cout << "Enter a report file: ";
  cin >> file_r;
  // If we are unable to open file, end program with error message
  ofstream ofs(file_r);
  if (!ofs)
  {
    cout << "Unable to open " << file_r << ".\n";
    return 0;
  }
  
  // Gettting start and end dates
  cout << "Enter report start date (mm dd yyyy): ";
  cin >> month_s >> day_s >> year_s;
  cout << "Enter report end date   (mm dd yyyy): ";
  cin >> month_e >> day_e >> year_e;
    
  // loop to organize data
  while (fin >> citation >> month >> day >> year >> speed >> speed_l >> road)
  {
    // Reseting fine
    fine = 0.00;
    // Calculating the fine
    fine = (speed - speed_l);
    switch (road)
    {
    case 'i':
      fine *= i;
      break;
    case 'r':
      fine *= r;
      break;
    case 'h':
      fine *= h;
      break;
    default:
      fine *= n;
      break;
    }
    // Fixing the year
    if (year < 100)
      year += 2000;

    // If the year is within the range
    if (year > year_s && year < year_e)
    {
      indate = true; 
    }
    // If the start year is the same check if the month (and then date if needed) is within the range
    else if (year == year_s)
    {
      if (month > month_s)
      {
        indate = true;
      }
      else if (month == month_s)
      {
        if (day >= day_s)
        {
          indate = true;
        }
      }
    }
    // If the end year is the same check if the month (and then date if needed) is within the range
    else if (year == year_e)
    {
      if (month < month_e)
      {
        indate = true;
      }
      else if (month == month_e)
      {
        if (day <= day_e)
        {
          indate = true;
        }
      }
    }
    else 
      indate = false;
    
    // If the date for the citation is within the time frame, write the info to the output file
    if (indate == true)
    {
      ofs << setw(2) << setfill('0') << day << "-" << m_name[month-1] << "-" << year << " " << left << setw(11) << setfill(' ') << citation << "$" << right << setw(9) << fixed << setprecision(2) << fine << '\n';
    } 
  }
  // Closing file
  fin.close();
  
  return 0;
}